<?php

namespace Drupal\payment_vendor_ecpay;

use Drupal\payment_vendor\Controller as Base;
use Payment;
use PaymentMethod;

/** Class Controller */
class Controller extends Base {

  /** Controller constructor. */
  function __construct() {
    parent::__construct();
    $this->title = t('ECPay');
    $this->description = t('ECPay');
  }

  /**
   * @param Payment $payment
   *
   * @return bool
   */
  function execute(Payment $payment) {
    parent::execute($payment);
  }

  /** @return Manager */
  function getManager() {
    return Manager::getInstance();
  }

  /**
   * @param Payment $payment
   * @param PaymentMethod $method
   * @param bool $strict
   */
  function validate(Payment $payment, PaymentMethod $method, $strict) {
    parent::validate($payment, $method, $strict);
  }

}
