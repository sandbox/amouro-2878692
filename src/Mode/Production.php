<?php

namespace Drupal\payment_vendor_ecpay\Mode;

use Drupal\payment_vendor\Mode\Production as Base;

/** Class Production */
class Production extends Base {

  /** @return array */
  protected function makeLinks() {
    return [
        'payment' => 'https://payment.ecpay.com.tw/Cashier/AioCheckOut/V5',
        'query' => 'https://payment.ecpay.com.tw/Cashier/QueryTradeInfo/V5',
        'capture' => 'https://payment.ecpay.com.tw/Cashier/Capture',
        'trade' => 'https://vendor.ecpay.com.tw/PaymentMedia/TradeNoAio',
        'credit_period' => 'https://payment.ecpay.com.tw/Cashier/QueryCreditCardPeriodInfo',
        'credit_action' => 'https://payment.ecpay.com.tw/CreditDetail/DoAction',
        'credit_detail' => 'https://payment.ecPay.com.tw/CreditDetail/QueryTrade/V2',
        'credit_trade' => 'https://payment.ecPay.com.tw/CreditDetail/FundingReconDetail',
      ] + parent::makeLinks();
  }

}
