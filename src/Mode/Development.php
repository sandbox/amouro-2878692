<?php

namespace Drupal\payment_vendor_ecpay\Mode;

use Drupal\payment_vendor\Mode\Development as Base;
use Payment;

/** Class Development */
class Development extends Base {

  /**
   * @param Payment $payment
   * @param array $items
   *
   * @return array
   */
  function getItems(Payment $payment, array $items) {
    $items = parent::getItems($payment, $items);
    return $items;
  }

  /** @return array */
  protected function makeLinks() {
    return [
        'payment' => 'https://payment-stage.ecpay.com.tw/Cashier/AioCheckOut/V5',
        'query' => 'https://payment-stage.ecpay.com.tw/Cashier/QueryTradeInfo/V5',
        'capture' => 'https://payment-stage.ecpay.com.tw/Cashier/Capture',
        'trade' => 'https://vendor-stage.ecpay.com.tw/PaymentMedia/TradeNoAio',
        'credit_period' => 'https://payment-stage.ecpay.com.tw/Cashier/QueryCreditCardPeriodInfo',
        'credit_action' => '',
        'credit_detail' => '',
        'credit_trade' => '',
      ] + parent::makeLinks();
  }

  /** @return array */
  protected function makeMethodOptions() {
    return [
        'merchant_code' => '2000132',
        'merchant_key' => '5294y06JbISpM5x9',
        'merchant_hash' => 'v77hoKGq4kWxNNIS',
        'platform_code' => '',
      ] + parent::makeMethodOptions();
  }

}
