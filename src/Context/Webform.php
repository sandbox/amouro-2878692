<?php

namespace Drupal\payment_vendor_ecpay\Context;

use Drupal\payment_vendor\Context\Webform as Base;

/** Class Webform */
class Webform extends Base {
  /** @return array */
  protected function makeViewItems() {
    $form = parent::makeViewItems();
    $wrapper = $this->wrapper;
    $payment = $wrapper->getContextForm('payment');
    $method = $wrapper->getManager()->getType($payment['method']);

    if (false == isset($method)) {
      return $form;
    }

    $form = $method->formOrderView($wrapper) + $form;
    return $form;
  }

}
