<?php

namespace Drupal\payment_vendor_ecpay\Route;

use Drupal\payment_vendor\Helper;
use Drupal\payment_vendor\Wrapper\PaymentWrapper;

/** Class Finish */
class Finish extends Route {

  /**
   * @param PaymentWrapper $item
   *
   * @return mixed
   */
  function execute(PaymentWrapper $item) {
    $form = parent::execute($item);
    $item->getPayment()->finish();
    return $form;
  }

  /** @return mixed */
  protected function failed() {
    return parent::failed();
  }

  /** @return mixed */
  protected function success() {
    $form = parent::success();
    $this->updatePayment()->doMethodHook();
    return $form;
  }

  /** @return static */
  protected function updatePayment() {
    $wrapper = $this->wrapper;
    $type = $this->isSuccessCode() ? 'status' : 'error';
    Helper::setMessage($this->getReturnMessage(), $type, true);
    $wrapper->pushContextRoute('finish', $this->items);
    $wrapper->save();
    return $this;
  }

  /** @return $this */
  protected function doMethodHook() {
    $type = $this->getType();

    if (isset($type)) {
      $type->doRouteFinish($this->wrapper);
    }

    return $this;
  }

  /** @return string */
  protected function getPaymentType() {
    $items = explode('_', $this->items['PaymentType'], 2);
    return empty($items) ? '' : reset($items);
  }

}
