<?php

namespace Drupal\payment_vendor_ecpay\Route;

use Drupal\payment_vendor\Helper;
use Drupal\payment_vendor\Route\Route as Base;
use Drupal\payment_vendor\Type\Type;
use Drupal\payment_vendor_ecpay\CheckCode;
use Drupal\payment_vendor_ecpay\Controller;

/** Class Route */
abstract class Route extends Base {

  /** @noinspection PhpMissingParentCallCommonInspection */
  protected function makeItems() {
    return $_POST;
  }

  /** @return bool */
  protected function validate() {
    if (false == parent::validate()) {
      return false;
    }

    if (false == $this->validateCheckCode()) {
      Helper::setMessage(t('CheckMacValue verify fail.'), 'error', true);
      return false;
    }

    return true;
  }

  /** @return string */
  protected function makeCheckCode() {
    $method = $this->wrapper->getContextMethod();
    return CheckCode::create()->make($this->items, $method['merchant_key'], $method['merchant_hash']);
  }

  /** @return bool */
  protected function validateCheckCode() {
    $code = &$this->items['CheckMacValue'];
    return (isset($code) && ($this->makeCheckCode() == $code));
  }

  /** @return bool */
  protected function isSuccessCode() {
    $item = &$this->items['RtnCode'];
    $code = isset($item) ? $item : 0;
    return (0 < $code);
  }

  /** @return string */
  protected function getReturnMessage() {
    $item = &$this->items['RtnMsg'];
    return isset($item) ? $item : '';
  }

  /** @return Controller */
  protected function getController() {
    /** @var Controller $item */
    $item = $this->wrapper->getController();
    return $item;
  }

  /** @return Type */
  protected function getType() {
    $wrapper = $this->wrapper;
    $items = $wrapper->getContextForm('payment');
    return $wrapper->getManager()->getType($items['method']);
  }
}
