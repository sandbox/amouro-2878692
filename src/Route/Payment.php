<?php

namespace Drupal\payment_vendor_ecpay\Route;

use Drupal\payment_vendor\Helper;
use Drupal\payment_vendor\Status\Status;
use Drupal\payment_vendor\Wrapper\PaymentWrapper;

/** Class Payment */
class Payment extends Route {

  /**
   * @param PaymentWrapper $item
   *
   * @return void
   */
  function execute(PaymentWrapper $item) {
    echo parent::execute($item);
    exit;
  }

  /** @noinspection PhpMissingParentCallCommonInspection
   * @return string
   */
  protected function failed() {
    return '0';
  }

  /** @noinspection PhpMissingParentCallCommonInspection
   * @return string
   */
  protected function success() {
    $isSuccessCode = $this->isSuccessCode();
    $status = $isSuccessCode ? Status::PAYMENT_STATUS_SUCCESS : Status::PAYMENT_STATUS_FAILED;
    $type = $isSuccessCode ? 'status' : 'error';
    Helper::setMessage($this->getReturnMessage(), $type, true);

    $wrapper = $this->wrapper;
    $wrapper->pushContextRoute('payment', $this->items);
    $wrapper->save($status);
    return '1|OK';
  }

  /** @noinspection PhpMissingParentCallCommonInspection
   * @return string
   */
  function title() {
    return t('Payment Route');
  }

  /** @return array */
  function defaultItems() {
    return [
      'MerchantID' => '',
      'MerchantTradeNo' => '',
      'StoreID' => '',
      'RtnCode' => 0,
      'RtnMsg' => '',
      'TradeNo' => '',
      'TradeAmt' => 0,
      'PaymentDate' => '',
      'PaymentType' => '',
      'PaymentTypeChargeFee' => 0,
      'TradeDate' => '',
      'SimulatePaid' => 1,
      'CustomField1' => '',
      'CustomField2' => '',
      'CustomField3' => '',
      'CustomField4' => '',
      'CheckMacValue' => '',
    ];
  }


  /** @return array */
  //  protected function sanitizeItems() {
  //    $items = [];
  //
  //    foreach($this->items as $index => $item) {
  //      switch ($index) {
  //        case 'PaymentType':
  //          $item = $this->sanitizePaymentType($item);
  //          break;
  //        case 'PeriodType':
  //          $item = $this->sanitizePeriodType($item);
  //          break;
  //      }
  //
  //      $item[$index] = $item;
  //    }
  //
  //    return $items;
  //
  //  }

  /**
   * @param string $item
   *
   * @return string
   */
  //  protected function sanitizePaymentType($item){
  //    $items = explode('_', $item, 2);
  //    return empty($items) ? '' : reset($items);
  //  }

  /**
   * @param string $item
   *
   * @return string
   */
  //  protected function sanitizePeriodType($item) {
  //    $search = ['Y', 'M', 'D'];
  //    $replace = ['Year', 'Month', 'Day'];
  //    return str_replace($search, $replace, $item);
  //  }

}
