<?php

namespace Drupal\payment_vendor_ecpay;

use Drupal\payment_vendor\Manager as Base;

/** Class Manager */
class Manager extends Base {

  /** @return array */
  protected function makeClassMap() {
    $items = parent::makeClassMap();
    $items['Form'] = ['method' => Form\Method::class, 'payment' => Form\Payment::class] + $items['Form'];
    $items['Action'] = ['payment' => Action\Payment::class] + $items['Action'];
    $items['Mode'] = [0 => Mode\Development::class, 1 => Mode\Production::class];
    $items['Route'] = ['payment' => Route\Payment::class, 'finish' => Route\Finish::class] + $items['Route'];
    $items['Type'] = [
        'Credit' => Type\Credit::class,
        'WebATM' => Type\WebATM::class,
        'ATM' => Type\ATM::class,
        'CVS' => Type\CVS::class,
      ] + $items['Type'];
    $items['Context'] = [
        'payment_ubercart' => Context\Ubercart::class,
        'payment_commerce' => Context\Commerce::class,
        'payment_webform' => Context\Webform::class,
      ] + $items['Context'];

    return $items;
  }

  /** @return array */
  protected function makeDefaults() {
    $items = parent::makeDefaults();

    $items['Form']['method'] = [
        'merchant_code' => '',
        'merchant_key' => '',
        'merchant_hash' => '',
        'platform_code' => '',
        'allowed_types' => [],
      ] + $items['Form']['method'];

    $items['Form']['payment'] = ['method' => null, 'installment' => 1, 'currency' => 'TWD'] + $items['Form']['payment'];
    return $items;
  }

  /** @return array */
  protected function makeOptions() {
    $items = parent::makeOptions();
    $items['Type'] = [
      'Credit' => t('Credit'),
      'WebATM' => t('WebATM'),
      'ATM' => t('ATM'),
      'CVS' => t('CVS'),
    ];
    return $items;
  }

}
