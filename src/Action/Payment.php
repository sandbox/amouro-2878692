<?php

namespace Drupal\payment_vendor_ecpay\Action;

use Drupal\payment_vendor\Action\Payment as Base;
use Drupal\payment_vendor\Wrapper\PaymentWrapper;
use Drupal\payment_vendor_ecpay\CheckCode;
use Drupal\payment_vendor_ecpay\Controller;

/** Class Payment */
class Payment extends Base {

  /**
   * @param PaymentWrapper $item
   *
   * @return bool
   */
  function execute(PaymentWrapper $item) {
    if (false == parent::execute($item)) {
      return false;
    }

    $this->autoSubmitPage();
    return true;
  }

  /** @return array */
  protected function makeItems() {
    $wrapper = $this->wrapper;
    $method = $wrapper->getContextMethod();
    $payment = $wrapper->getContextForm('payment');

    $items = $wrapper->getManager()->getType($payment['method'])->makePaymentItems($wrapper) + [
        'MerchantID' => $method['merchant_code'],
        'MerchantTradeNo' => $this->makeTradeNumber(),
        'TotalAmount' => $payment['total'],
        'ItemName' => $this->makeItemName(),
        'ChoosePayment' => $payment['method'],
        //        'NeedExtraPaidInfo' => 'Y',
        'EncryptType' => CheckCode::ENCRYPT_TYPE_MD5,
      ] + parent::makeItems() + $this->defaultItems();

    $items['CheckMacValue'] = CheckCode::create()->make($items, $method['merchant_key'], $method['merchant_hash']);
    return $items;
  }

  /** @return Controller */
  protected function getController() {
    /** @var Controller $item */
    $item = $this->wrapper->getController();
    return $item;
  }

  protected function autoSubmitPage() {
    $wrapper = $this->wrapper;
    $method = $wrapper->getContextMethod();

    $html = [
      '#theme' => 'payment_vendor_auto_submit',
      '#type' => 'html',
      '#action' => $wrapper->getController()->getManager()->getMode($method['execute_mode'])->getLink('payment'),
      '#items' => $this->items,
    ];

    print drupal_render($html);
    drupal_exit();
    exit;
  }

  /** @return array */
  protected function defaultItems() {
    return [
      'MerchantID' => '',
      'MerchantTradeNo' => '',
      'MerchantTradeDate' => date('Y/m/d H:i:s'),
      'PaymentType' => 'aio',
      'TotalAmount' => 0,
      'TradeDesc' => 'Drupal Payment Vendor ECPay',
      'ItemName' => '',
      'ReturnURL' => url('payment_vendor/' . $this->wrapper->getPayment()->pid . '/payment', ['absolute' => true]),
      'ChoosePayment' => 'ALL',
      //      'CheckMacValue' => '',
      //      'EncryptType' => CheckCode::ENCRYPT_TYPE_SHA256,
    ];
  }

  /** @return string */
  protected function makeTradeNumber() {
    return sprintf('%04X%016X', mt_rand(0, 65535), time());
  }

  /** @return string */
  protected function makeItemName() {
    $items = [];

    foreach ($this->wrapper->getPayment()->getLineItems() as $item) {
      $text = str_replace('#', ' ', $item->description);
      $text = trim($text);
      $items[] = $text;
    }

    $text = implode('#', $items);
    $text = substr($text, 0, 200);
    return $text;
  }

}
