<?php

namespace Drupal\payment_vendor_ecpay\Form;

use Drupal\payment_vendor\Currency\Currency;
use Drupal\payment_vendor\Form\Payment as Base;
use Drupal\payment_vendor\Helper;
use Drupal\payment_vendor_ecpay\Controller;
use Drupal\payment_vendor_ecpay\Type\Credit;

/** Class Payment */
class Payment extends Base {

  /** @return array */
  protected function doForm() {
    return ['method' => $this->formMethod(), 'installment' => $this->formInstallment()] + parent::doForm();
  }

  /** @return Currency */
  protected function getCurrency() {
    $item = parent::getCurrency();
    return $item;
  }

  /**
   * @param $item
   *
   * @return bool
   */
  protected function isController($item) {
    return parent::isController($item) && Helper::isObject($item, Controller::class);
  }

  /** @return array */
  protected function formMethod() {
    return [
      '#type' => 'select',
      '#title' => t('Payment Method'),
      '#options' => $this->wrapper->getManager()->getOptions('Type'),
      '#default_value' => $this->values['method'],
      '#required' => true,
    ];
  }

  /** @return array */
  protected function formInstallment() {
    $wrapper = $this->wrapper;
    $manager = $wrapper->getManager();

    /** @var Credit $credit */
    $credit = $manager->getType('Credit');
    $name = $manager->getContext($wrapper->getContextName())->getFieldNamePrefix() . '[method]';
    $index = ':input[name="' . $name . '"]';

    return [
      '#type' => 'select',
      '#title' => t('Installment'),
      '#description' => t('<h3>Cooperate Installment Banks:</h3><ul><li>Taishin International Bank</li><li>E. Sun Bank</li><li>Cathay United Bank</li><li>Far Eastern International Bank</li><li>SinoPac Bank</li></ul>'),
      '#options' => $credit->getInstallmentOptions(),
      '#default_value' => $this->values['installment'],
      '#required' => true,
      '#states' => ['visible' => [$index => ['value' => 'Credit']]],
    ];
  }
}
