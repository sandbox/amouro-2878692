<?php

namespace Drupal\payment_vendor_ecpay\Form;

use Drupal\payment_vendor\Form\Method as Base;
use Drupal\payment_vendor\Helper;
use Drupal\payment_vendor_ecpay\Controller;

/** Class Method */
class Method extends Base {

  /** @return array */
  protected function doForm() {
    $values = $this->values;
    $form = parent::doForm();

    $form['merchant_hash'] = [
      '#type' => 'textfield',
      '#title' => t('Merchant Hash'),
      '#default_value' => $values['merchant_hash'],
      '#required' => true,
    ];

    $form['platform_code'] = [
      '#type' => 'textfield',
      '#title' => t('Platform Code'),
      '#default_value' => $values['platform_code'],
    ];

    $form['allowed_types'] = [
      '#type' => 'checkboxes',
      '#title' => t('Allowed Payment Types'),
      '#options' => $this->getManager()->getOptions('Type'),
      '#default_value' => $values['allowed_types'],
      '#required' => true,
    ];

    $form['merchant_code'] = ['#title' => t('Merchant ID'), '#maxlength' => 10] + $form['merchant_code'];
    $form['merchant_key'] = ['#title' => t('Hash Key'), '#maxlength' => 16] + $form['merchant_key'];
    $form['merchant_hash'] = ['#title' => t('Hash IV'), '#maxlength' => 16] + $form['merchant_hash'];
    $form['platform_code'] = ['#title' => t('Platform ID'), '#maxlength' => 10] + $form['platform_code'];
    $form['allowed_types'] = ['#title' => t('Payment Methods')] + $form['allowed_types'];

    return $form;
  }

  /**
   * @param $item
   *
   * @return bool
   */
  protected function isController($item) {
    return parent::isController($item) && Helper::isObject($item, Controller::class);
  }

}
