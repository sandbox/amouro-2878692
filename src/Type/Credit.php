<?php

namespace Drupal\payment_vendor_ecpay\Type;

use Drupal\payment_vendor\Type\Type;
use Drupal\payment_vendor\Wrapper\PaymentWrapper;

/** Class Credit */
class Credit extends Type {

  /** @return array */
  function getInstallmentOptions() {
    $range = [1, 3, 6, 12, 18, 24];
    $items = array_combine($range, $range);
    return $items;
  }

  /**
   * @param PaymentWrapper $wrapper
   *
   * @return array
   */
  function formOrderView(PaymentWrapper $wrapper) {
    $form = parent::formOrderView($wrapper);
    $items = $this->wrapper->getContextForm('payment');
    $form['Installment'] = ['#title' => t('Installment'), '#markup' => $items['installment']];
    return $form;
  }

  /**
   * @param PaymentWrapper $wrapper
   *
   * @return array
   */
  function makePaymentItems(PaymentWrapper $wrapper) {
    $items = parent::makePaymentItems($wrapper);
    $items = ['OrderResultURL' => $this->getFinishLink()] + $items;

    $payment = $wrapper->getContextForm('payment');
    $installment = intval($payment['installment']);

    if (1 < $installment) {
      $items = [
          'CreditInstallment' => $installment,
          'InstallmentAmount' => $payment['total'],
        ] + $items;
    }

    return $items;
  }

  /** @noinspection PhpMissingParentCallCommonInspection
   * @return string
   */
  function title() {
    return t('Credit');
  }
}
