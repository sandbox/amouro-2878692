<?php

namespace Drupal\payment_vendor_ecpay\Type;

use Drupal\payment_vendor\Type\Type;
use Drupal\payment_vendor\Wrapper\PaymentWrapper;

/** Class ATM */
class ATM extends Type {

  /** @param PaymentWrapper $wrapper */
  function doRouteFinish(PaymentWrapper $wrapper) {
    parent::doRouteFinish($wrapper);
    $item = $this->getVirtualAccount();

    if ('' == $item) {
      return;
    }

    drupal_set_message(t('Virtual Account: @account', ['@account' => $item]));
  }

  /**
   * @param PaymentWrapper $wrapper
   *
   * @return array
   */
  function formOrderView(PaymentWrapper $wrapper) {
    $form = parent::formOrderView($wrapper);
    $items = $this->wrapper->getLastContextRoute('finish');

    if (false == $this->isSuccess($items)) {
      return $form;
    }

    $form['BankCode'] = ['#title' => t('Bank Code'), '#markup' => $items['BankCode']];
    $form['ExpireDate'] = ['#title' => t('Expire Date'), '#markup' => $items['ExpireDate']];
    $form['vAccount'] = ['#title' => t('Virtual Account'), '#markup' => $items['vAccount']];
    return $form;
  }

  /**
   * @param PaymentWrapper $wrapper
   *
   * @return array
   */
  function makePaymentItems(PaymentWrapper $wrapper) {
    $items = parent::makePaymentItems($wrapper);
    return ['ClientRedirectURL' => $this->getFinishLink()] + $items;
  }

  /** @noinspection PhpMissingParentCallCommonInspection
   * @return string
   */
  function title() {
    //  Automated Teller Machine
    return t('ATM');
  }

  /** @return string */
  protected function getVirtualAccount() {
    $items = $this->wrapper->getLastContextRoute('finish');
    $item = &$items['vAccount'];
    return isset($item) ? $item : '';
  }

}
