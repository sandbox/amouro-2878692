<?php

namespace Drupal\payment_vendor_ecpay\Type;

use Drupal\payment_vendor\Type\Type;
use Drupal\payment_vendor\Wrapper\PaymentWrapper;

/** Class WebATM */
class WebATM extends Type {

  /**
   * @param PaymentWrapper $wrapper
   *
   * @return array
   */
  function makePaymentItems(PaymentWrapper $wrapper) {
    $items = parent::makePaymentItems($wrapper);
    return ['OrderResultURL' => $this->getFinishLink()] + $items;
  }

  /** @noinspection PhpMissingParentCallCommonInspection
   * @return string
   */
  function title() {
    return t('WebATM');
  }

}
