<?php

namespace Drupal\payment_vendor_ecpay\Type;

use Drupal\payment_vendor\Type\Type;
use Drupal\payment_vendor\Wrapper\PaymentWrapper;

/** Class ConvenienceStore */
class CVS extends Type {

  /** @param PaymentWrapper $wrapper */
  function doRouteFinish(PaymentWrapper $wrapper) {
    parent::doRouteFinish($wrapper);
    $item = $this->getPaymentNumber();

    if ('' == $item) {
      return;
    }

    drupal_set_message(t('Payment Number: @number', ['@number' => $item]));
  }

  /**
   * @param PaymentWrapper $wrapper
   *
   * @return array
   */
  function formOrderView(PaymentWrapper $wrapper) {
    $form = parent::formOrderView($wrapper);
    $items = $this->wrapper->getLastContextRoute('finish');

    if (false == $this->isSuccess($items)) {
      return $form;
    }

    $form['ExpireDate'] = ['#title' => t('Expire Date'), '#markup' => $items['ExpireDate']];
    $form['PaymentNo'] = ['#title' => t('Payment Number'), '#markup' => $items['PaymentNo']];
    return $form;
  }

  /**
   * @param PaymentWrapper $wrapper
   *
   * @return array
   */
  function makePaymentItems(PaymentWrapper $wrapper) {
    $items = parent::makePaymentItems($wrapper);
    return ['ClientRedirectURL' => $this->getFinishLink()] + $items;
  }

  /** @noinspection PhpMissingParentCallCommonInspection
   * @return string
   */
  function title() {
    //  Convenience Store
    return t('CVS');
  }

  /** @return string */
  protected function getPaymentNumber() {
    $items = $this->wrapper->getLastContextRoute('finish');
    $item = &$items['PaymentNo'];
    return isset($item) ? $item : '';
  }

}
