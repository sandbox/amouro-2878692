<?php

namespace Drupal\payment_vendor_ecpay;

use Drupal\payment_vendor\Registry\RegistryItem;

/** Class ResponseCheckCode */
class CheckCode extends RegistryItem {
  const ENCRYPT_TYPE_MD5 = 0;
  const ENCRYPT_TYPE_SHA256 = 1;

  /** @var static */
  static protected $instance;
  protected $replaceTextMap = [];
  protected $items = [];

  protected $hash = '';
  protected $key = '';

  /** ResponseCheckCode constructor. */
  protected function __construct() {
    $this->replaceTextMap = $this->makeReplaceTextMap();
  }

  /** @noinspection PhpMissingParentCallCommonInspection */
  /** @return static */
  static function create() {
    $instance = &static::$instance;

    if (false == isset($instance)) {
      $instance = new static();
    }

    return $instance;
  }

  /** @return bool */
  static function test() {
    $key = '5294y06JbISpM5x9';
    $hash = 'v77hoKGq4kWxNNIS';

    $items = [
      'TradeDesc' => '促銷方案',
      'PaymentType' => 'aio',
      'MerchantTradeDate' => '2013/03/12 15:30:23',
      'MerchantTradeNo' => 'ecpay20130312153023',
      'MerchantID' => '2000132',
      'ReturnURL' => 'https://www.ecpay.com.tw/receive.php',
      'ItemName' => 'Apple iphone 7 手機殼',
      'TotalAmount' => '1000',
      'ChoosePayment' => 'ALL',
      'EncryptType' => '1',
    ];

    $code = static::create()->make($items, $key, $hash);
    return ('CFA9BDE377361FBDD8F160274930E815D1A8A2E3E80CE7D404C45FC9A0A1E407' == $code);
  }

  /**
   * @param array $items
   * @param string $key
   * @param string $hash
   *
   * @return string
   */
  function make(array $items, $key, $hash) {
    $this->load($items, $key, $hash);
    $text = $this->stepJoinItems();
    $text = $this->stepReplaceText($text);
    $text = $this->stepEncryptText($text);
    $text = strtoupper($text);
    return $text;
  }

  /**
   * @param string $left
   * @param string $right
   *
   * @return int
   */
  function sort($left, $right) {
    return strcasecmp($left, $right);
  }

  /**
   * @param array $items
   * @param string $key
   * @param string $hash
   */
  protected function load(array $items, $key, $hash) {
    unset($items['CheckMacValue']);
    uksort($items, [$this, 'sort']);
    $this->items = $items;
    $this->key = $key;
    $this->hash = $hash;
  }

  /** @return int */
  protected function getEncryptType() {
    $item = &$this->items['EncryptType'];
    return isset($item) ? intval($item) : 0;
  }

  /**
   * @param array $items
   *
   * @return string
   */
  protected function stepJoinItems() {
    $data = ['HashKey=' . $this->key];

    foreach ($this->items as $index => $item) {
      $data[] = $index . '=' . $item;
    }

    $data[] = 'HashIV=' . $this->hash;
    return implode('&', $data);
  }

  /**
   * @param string $text
   *
   * @return string
   */
  protected function stepReplaceText($text) {
    $map = &$this->replaceTextMap;
    $text = urlencode($text);
    $text = str_replace(array_keys($map), array_values($map), $text);
    $text = strtolower($text);
    return $text;
  }

  /**
   * @param string $text
   *
   * @return string
   */
  protected function stepEncryptText($text) {
    switch ($this->getEncryptType()) {
      case static::ENCRYPT_TYPE_SHA256:
        $text = hash('sha256', $text);
        break;

      case static::ENCRYPT_TYPE_MD5:
      default:
        $text = md5($text);
        break;
    }

    return $text;
  }

  /** @return array */
  protected function makeReplaceTextMap() {
    $items = [];
    $replace = ['-', '_', '.', '!', '*', '(', ')'];

    foreach ($replace as $item) {
      $items[urlencode($item)] = $item;
    }

    return $items;
  }

}
